<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\ChildImage;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        $this->call([
            RegionSeeder::class,
            OrganizationSeeder::class,
            ChildSeeder::class,
            ChildImageSeeder::class,
            VolunterSeeder::class,
            NewsSeeder::class,
            NewsCategorySeeder::class,
            GalleryImageSeeder::class,
            GalleryImageSeeder::class
        ]);
    }
}

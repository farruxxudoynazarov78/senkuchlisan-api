<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\News>
 */
class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'category_id' =>\App\Models\NewsCategory::factory()->create()->id,
            'title' => $this->faker->sentence,
            'content' => $this->faker->paragraph,
            'published_date' => $this->faker->date,
            'is_published' =>$this->faker->boolean
        ];
    }
}

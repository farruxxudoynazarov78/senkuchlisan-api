<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Child;
use App\Models\Region;
use App\Models\Organization;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Child>
 */
class ChildFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Child::class;
    public function definition(): array
    {
    
         return [
             'fio' => $this->faker->name,
             'region_id' => Region::inRandomOrder()->first()->id,
             'organization_id' => optional(Organization::inRandomOrder()->first())->id,
             'birth_date' => $this->faker->date,
             'diagnosis' => $this->faker->word,
             'about' => $this->faker->paragraph,
             'contact_fio' => $this->faker->name,
             'contact_number' => $this->faker->phoneNumber,
             'is_alive'=>$this->faker->boolean(95),
             'created_at' => now(),
             'updated_at' => now(),
         ];
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Child;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ChildImage>
 */
class ChildImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
           'child_id' => Child::factory()->create()->id,
           'file_path' => $this->faker->imageUrl(),
           'price' => $this->faker->randomFloat(2,0,100),
           'is_active' => $this->faker->boolean(),
           'likes_count' => $this->faker->numberBetween(0, 100),
        ];
    }
}

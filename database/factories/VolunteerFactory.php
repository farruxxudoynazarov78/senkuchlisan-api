<?php

namespace Database\Factories;

use App\Http\Controllers\Api\VolunteerController;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Volunteer;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Volunteer>
 */
class VolunteerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

     protected $model = Volunteer::class;

    public function definition(): array
    {
        return [
            'fio' => $this->faker->name,
            'birth_year' => $this->faker->numberBetween(1990, 2005),
            'avatar' => $this->faker->imageUrl(),
            'workplace' => $this->faker->company,
            'region_id' =>mt_rand(1,10),
            // 'education' => $this->faker->randomElement(['College', 'University', 'None']),
        ];
    }
}

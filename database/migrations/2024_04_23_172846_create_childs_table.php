<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('childs', function (Blueprint $table) {
            $table->id();
            $table->string('fio');
            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('organization_id');
            $table->date('birth_date');
            $table->string('diagnosis');
            $table->text('about');
            $table->string('contact_fio')->nullable();
            $table->string('contact_number')->nullable();
            $table->boolean('is_alive')->default(true);
            $table->timestamps();
            
            $table->foreign('region_id')->references('id')->on('regions');
            $table->foreign('organization_id')->references('id')->on('organizations');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('childs');
    }
};

<?php

namespace App\Http\Resources;

use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChildResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {

        return [
            'id' => $this->id,
            'fio' => $this->fio,
            'birth_date' => $this->birth_date->format('Y-m-d'),
            'diagnosis' =>$this->diagnosis,
            'about' => $this->about,
            'contact_fio' => $this->contact_fio,
            'contact_number' => $this->contact_number,
            'is_alive' => $this->is_alive,
            
            // 'region' => new RegionResource($this->region),
            // 'organization' => new OrganizationResource($this->organization),
        ];

    }
}

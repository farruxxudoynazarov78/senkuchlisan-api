<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChildUpdateRequest;
use App\Http\Requests\StoreChildRequest;
use Illuminate\Http\Request;
use App\Models\Child;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ChildController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $child = Child::with('region:id,name', 'organization:id,name')->get();
        return response()->json($child);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreChildRequest $request)
    {

        $child = Child::create($request->all());
        return response()->json($child, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        try{
            $child = Child::with('region:id,name', 'organization:id,name')->get();
            return response()->json($child);
        } catch(ModelNotFoundException $e){
           return response()->json(['message' => 'Child not fount']);
        }
    }

    /**
     * Update the specified resource in storage.
     */ 
    public function update(ChildUpdateRequest $request, int $id)
    {

        try{
            $child = Child::findOrFail($id);
            $child->update($request->all());
            return response()->json($child);
        }  catch(ModelNotFoundException $e){
            return response()->json(['message' =>  'Child not found'], 404);
        }
        
    
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
       
        try{
            $child = Child::find($id);
            $child->delete();
            return response()->json(['message' => 'Child deleted succesfully'], 204);
        } catch(ModelNotFoundException $e){
            return response()->json(['message' => 'Child not found']);
        } catch(\Exception $e){
            return response()->json(['message' => 'An error deleting the child'], 500);
        }
    }
}

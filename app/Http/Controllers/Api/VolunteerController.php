<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VolunteerRequest;
use App\Models\Volunteer;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;


class VolunteerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
      $volunter = Volunteer::with('region:id,name')->get();
      return response()->json($volunter);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VolunteerRequest $request)
    {

        
        $volunteer = Volunteer::create($request->all());
        return response()->json($volunteer, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $volunteer = Volunteer::with('region:id,name')->findOrFail($id);
        return response()->json($volunteer);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VolunteerRequest $request, string $id)
    {
        $volunteer = Volunteer::findOrFail($id);
        $volunteer->update($request->all());
        return response()->json($volunteer);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
      try{
        $volunteer = Volunteer::findOrFail($id);
        $volunteer->delete();
        return response()->json($volunteer);
      }
      catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
        return response()->json(['error' => 'Malumot topilmadi'], 402);
      }
      catch(\Exception $e) {
        return response()->json(['error' => 'Xatolik yuz berdi'],500);
      }

    }
}

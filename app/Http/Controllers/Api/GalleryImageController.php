<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\GalleryImage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Models\ChildImage;

class GalleryImageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {   
        $galleyrimage = GalleryImage::with('gallery:id,name')->get();
        return response()->json($galleyrimage);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'gallery_id' => 'required|exists:galleries,id', 
            'file' => 'required|image|max:2048', 
        ]);


        if ($request->hasFile('file') && $request->file('file')->isValid()) {

            $path = $request->file('file')->store('gallery_images');
            $galleryImage = GalleryImage::create([
                'gallery_id' => $request->gallery_id,
                'file_path' => $path,
            ]);
            return response()->json($galleryImage, 201);

        } else {
            return response()->json(['error' => 'Fayl yuklashda xatolik'], 400);
        }
    }   
    
    



    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        try{
            $galleryImage = GalleryImage::findOrFail($id);
            return response()->json($galleryImage);
        }  catch(ModelNotFoundException $e){
            return response()->json(['message' => 'Gallery img not fount'], 404);
        } 
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id)
    {

        $request->validate([
            'gallery_id' => 'required',
            'file_path' => 'required'
        ]);

        try{
            $galleryImage = GalleryImage::findOrFail($id);
            $galleryImage->update($request->all());
            return response()->json($galleryImage);
        }
        catch(ModelNotFoundException $e){
            return response()->json(['message' =>'Gallery image not found'], 404);
        }
       
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        try{
            $galleryimage = GalleryImage::findOrFail($id);
            $galleryimage->delete();
            return response()->json(['message' => 'Gallery  image deleted successfully']);
        }
        catch(ModelNotFoundException $e){
return response()->json(['message' => 'Gallery  image not fount'], 404);
        }
        catch(\Exception $e){
return response()->json(['message' => 'An error ocurred while deleting the gallery image']);
        }
    }
}

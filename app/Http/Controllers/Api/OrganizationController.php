<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Models\Child;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {


        $organization = Organization::with('childs')->get();
        
        // Ma'lumotlarni JSON formatida chiqarish
        return response()->json($organization);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $organization = Organization::create($request->all());
        return response()->json($organization, 201);

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $organization = Organization::findOrFail($id);
        return response()->json($organization);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {

        $organization = Organization::findOrFail($id);
        $organization->update($request->all());
        return response()->json($organization);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        try{
           $organization = Organization::findOrFail($id);
           $organization->delete();
            return response()->json(['message'=> 'Organization deleted succesfully'], 204);
        } catch(ModelNotFoundException $e){
            return response()->json(['message' => 'Organization not found'], 404);
        } catch(\Exception $e) {
            return response()->json(['message' => 'Organization An error occurred while deleting the organization'], 404);
        }
    }
}

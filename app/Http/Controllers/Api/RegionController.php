<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Child;
use Illuminate\Http\Request;
use App\Models\Region;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $region = Child::with('childs')->get();
        
        // Ma'lumotlarni JSON formatida chiqarish
        return response()->json($region);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $region = Region::create($request->all());
        return response()->json($region, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $region = Region::with('childs')->find($id);
        return response()->json($region);
    }
          
    /**
     * Update the specified resource in storage.
     */
        // PUT /regions/{id}

    public function update(Request $request, string $id)
    {
       $region = Region::findOrFail($id);
       $region->update($request->all());
       return response()->json($region);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Region::destroy($id);
        return response()->json(null, 204);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;
use Illuminate\Http\Request;
use App\Models\News;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $news = News::with('category:id,name')->get();
        return response()->json($news);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NewsRequest $request)
    {
       

        $news = News::create($request->all());
        return response()->json($news, 201);

    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        try{
           $news = News::with('category:id,name')->findOrFail($id);
           return response()->json($news); 
        } catch(ModelNotFoundException $e){
            return response()->json(['message' => 'Newws Not Found'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NewsRequest $request, int $id)
    {
        
        try{
            $news = News::findOrFail($id);
            $news->update($request->all());
            return response()->json($news);
        }
        catch(ModelNotFoundException $e){
            return response()->json(['message' => 'News Not found'], 404);
        }
       
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        try{
            $news =  News::findOrFail($id);
            $news->delete();
            return response()->json(['message'=> "News deleted successfully"]);
        } catch(ModelNotFoundException $e){
            return response()->json(['message' => 'News not found'], 404);
        }
        catch(\Exception $e){
            return response()->json(['message' => 'An error occurred while deleting the news'], 500);
        }
    }
}

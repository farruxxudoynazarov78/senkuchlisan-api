<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsCategoryRequest;
use App\Models\NewsCategory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class NewsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return NewsCategory::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(NewsCategoryRequest $request)
    {

        $validateData = $request->validate([
            'name' => 'required'
        ]);

        $newscategory = NewsCategory::create($validateData);
        return response()->json($newscategory, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        try{
            $newsCategory = NewsCategory::findOrFail($id);
            return response()->json($newsCategory);
        } catch(ModelNotFoundException $e){
            return response()->json(['message' => 'News Category not fount'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(NewsCategoryRequest $request, int $id)
    {
       try{
        $newsCategory = NewsCategory::findOrFail($id);
        $newsCategory->update($request->all());
        return response()->json($newsCategory);
       }
       catch(ModelNotFoundException $e){
        return response()->json(['message' => 'News Category not found'], 404);
       }
   }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
            try {
                $newscategory = NewsCategory::findOrFail($id);
                $newscategory->delete();
                return response()->json(['message' => 'News category deleted successfully']);
            } catch (ModelNotFoundException $e) {
                return response()->json(['message' => 'News Category not found', 404]);
            } catch (\Exception $e) {
                return response()->json(['message' => 'An error occurred while deleting the news category'], 500);
            }
    }
}

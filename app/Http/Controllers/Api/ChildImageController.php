<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreChildImageRequest;
use Illuminate\Http\Request;
use App\Models\ChildImage;
use App\Models\GalleryImage;

class ChildImageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $childImage = ChildImage::with('child:id,fio')->get();
        return response()->json($childImage);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreChildImageRequest $request)
    {

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            // Faylni saqlash uchun nom va yo'lni o'zi tayinlaymiz
            $path = $request->file('file')->store('child_images');
    
            // Faylni serverga saqlash va yangi obyekt yaratish
            $childImage = ChildImage::create([
                'child_id' => $request->child_id,
                'file_path' => $path,
                'price' => $request->price,
                'is_active' => $request->is_active,
                'likes_count' => $request->likes_count ?? 0,
            ]);
    
            // Faylni muvaffaqiyatli saqlash haqida habar qaytarish
            return response()->json($childImage, 201);
        } else {
            // Faylni qabul qilishda xatolik bo'lsa, 400 status kodi bilan xatolik haqida habar qaytarish
            return response()->json(['error' => 'Fayl yuklashda xatolik'], 400);
        }
    }   
    
    
    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $childImage = ChildImage::with('child:id,fio')->findOrFail($id);
        return response()->json($childImage);

    }

    /**     
     * Update the specified resource in storage.
     */
    public function update(StoreChildImageRequest $request, int $id)
    {
       
        $childImage = ChildImage::findOrFail($id);

        $childImage->update([
            'child_id' => $request->input('child_id'),
            'file_path' => $request->input('file_path'),
            'price' => $request->input('price'),
            'is_active' => $request->input('is_active', true),
            'likes_count' => $request->input('likes_count', 0),
        ]);


        return response()->json($childImage, 200 );

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        try{
            $childImage = ChildImage::findOrFail($id);
            $childImage->delete();
            return response()->json(['message'=> "Child Image deleted successfully"]);
        }
        catch(\Exception $e){
           return response()->json(['message' => 'Child image not found'], 404);
        }
    }
}

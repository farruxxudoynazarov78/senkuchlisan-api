<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryRequest;
use App\Models\Gallery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Gallery::all();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(GalleryRequest $request)
    {
        $request->validate([
            'name' =>'required',
             'description'=> 'nullable',
             'is_active' => 'boolean'
        ]);

        $gallery = Gallery::create($request->all());
        return response()->json($gallery, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $gallery = Gallery::findOrFail($id);
        return response()->json($gallery);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(GalleryRequest $request, int $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'is_active' => 'boolean'
        ]);

        try{
            $gallery = Gallery::findOrFail($id);
            $gallery->update($request->all());
            return response()->json($gallery);
        } catch(ModelNotFoundException $e){
            return response()->json(['message' => 'Gallery not found'], 404);
        } catch(\Exception $e) {
            return response()->json(['message'=> 'An error occurred while updating the Gallery'], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        try{
         $gallery = Gallery::findOrFail($id);
         $gallery->delete();
         return response()->json($gallery);  
        }
        catch(ModelNotFoundException $e){
        return response()->json(['message' => 'Gallery not found'], 404);
    }     catch(\Exception $e){
        return response()->json(['message' => 'An eror occured while deleting the Gallery']);
    }
}
}

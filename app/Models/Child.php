<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    use HasFactory;

    protected $table = 'childs';
    
    protected $fillable = [
        'fio', 'region_id', 'organization_id', 'birth_date', 'diagnosis',
        'about', 'contact_fio', 'contact_number', 'is_alive'
    ];

    public function region(){
        return $this->belongsTo(Region::class);
    }

    public function organization(){
        return $this->belongsTo(Organization::class);
    }

    public function childImages(){
        return $this->hasMany(ChildImage::class);
    }


}

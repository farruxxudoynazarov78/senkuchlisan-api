<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'child_id', 'file_path', 'price', 'is_active', 'like_count'
    ];

    public function child(){
        return $this->belongsTo(Child::class);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    use HasFactory;

    protected $fillable = [
        'fio', 'birth_year', 'avatar', 'workplace', 'region_id', 'education'
    ];

    public function region(){
        return $this->belongsTo(Region::class);
    }


    
}

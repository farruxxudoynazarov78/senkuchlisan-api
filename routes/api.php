<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


use App\Http\Controllers\Api\ChildController;
use App\Http\Controllers\Api\ChildImageController;
use App\Http\Controllers\Api\GalleryController;
use App\Http\Controllers\Api\GalleryImageController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\NewsCategoryController;
use App\Http\Controllers\Api\OrganizationController;
use App\Http\Controllers\Api\VolunteerController;
use App\Http\Controllers\Api\RegionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('children', ChildController::class);
Route::apiResource('childimage', ChildImageController::class);
Route::apiResource('gallery', GalleryController::class);
Route::apiResource('galleryimg', GalleryImageController::class);
Route::apiResource('news', NewsController::class);
Route::apiResource('regions', RegionController::class);
Route::apiResource('newscategory', NewsCategoryController::class);
Route::apiResource('organization', OrganizationController::class);
Route::apiResource('volunter', VolunteerController::class);



